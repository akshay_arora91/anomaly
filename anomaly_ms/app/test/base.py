
# from flask_testing import TestCase
from unittest import TestCase

from app.main import config, db
from manage import app

# getting testing config from config.py
testing_config = config.TestingConfig


class BaseTestCase(TestCase):
    """ Base Tests """

    def create_app(self):
        app.config.from_object('app.main.config.TestingConfig')
        app.config['TESTING'] = testing_config.TESTING
        app.config['WTF_CSRF_ENABLED'] = False
        app.config['SQLALCHEMY_DATABASE_URI'] = testing_config.SQLALCHEMY_DATABASE_URI

        return app

    def setUp(self):
        # app = create_app(os.getenv('APPLICATION_ENV') or 'test')
        # app.config['TESTING'] = testing_config.TESTING
        # app.config['WTF_CSRF_ENABLED'] = False
        # app.config['SQLALCHEMY_DATABASE_URI'] = testing_config.SQLALCHEMY_DATABASE_URI
        db.create_all()

    def tearDown(self):
        db.session.remove()
        db.drop_all()
