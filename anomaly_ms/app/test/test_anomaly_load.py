import io
import json
import os
import unittest
import urllib
from app.main import db
from app.test.base import BaseTestCase
from manage import app

from app.main.util.messages import (LOAD_SUCCESS_MESSAGE,
                                    MANDANTORY_FIELD_REQUIRED_ERROR_MESSAGE,
                                    IP_PATH_OR_FORM_DATA_MANDANTORY_ERROR_MESSAGE)

load_url = "/api/point_anomaly/score"
test_client = app.test_client()

current_dir = os.path.dirname(os.path.abspath(__file__))
csv_file_path = os.path.join(current_dir, 'csvfiles/anomaly_input.csv')
csv_file_path_output = os.path.join(current_dir, 'csvfiles/exp_avg_output.csv')

# function to urlencode a query string


def get_url_encode(data):
    urlencoded_data = urllib.parse.urlencode(data)
    return urlencoded_data

# function to make post call with parameters


def load_post_call(historical=None, newpoint=None, pickle=None, urlencode=None):
    data = dict(
        # data = data
        historical=historical,
        newpoint=newpoint,
        pickle=pickle
    )
    if urlencode:
        data = get_url_encode(data)
    response = test_client.post(
        load_url,
        data=data,
        content_type='multipart/form-data',
        follow_redirects=True
    )
    return response


class TestLoad(BaseTestCase):
    pass
    # testing status code for a successful post call
    # def test_load_successpost(self):
    #     response = load_post_call(historical=None, newpoint=None, pickle=None, urlencode=True)
    #     self.assertTrue(response.json['message'] == LOAD_SUCCESS_MESSAGE)
    #     self.assertEqual(response.json['status_code'], 201)

    # # test case to validate data types in fields
    # def test_load_uploading_non_csv(self):
    #     response = load_post_call()
    #     self.assertTrue(response.json['message'] == 'non csv file uploaded')
    #     self.assertEqual(response.json['status_code'], 201)

    # # invalid data type for ip_path
    # def test_load_invalid_extension_in_ip_path(self):
    #     response = bitmap_detector_post_call()
    #     self.assertTrue(response.json['message'] == 'non csv file uploaded')
    #     self.assertEqual(response.json['status_code'], 201)

    # # invalid data type for op_path
    # def test_load_invalid_extension_in_op_path(self):
    #     response = bitmap_detector_post_call()
    #     self.assertTrue(response.json['message'] == 'non csv file specified')
    #     self.assertEqual(response.json['status_code'], 201)

    # # invalid data type for data
    # def test_load_uploading_non_csv_file(self):
    #     response = bitmap_detector_post_call()
    #     self.assertTrue(response.json['message'] == 'non csv file uploaded')
    #     self.assertEqual(response.json['status_code'], 201)

    # # uploading csv file with invalid columns
    # def test_load_uploading_csv_with_invalid_columns(self):
    #     response = bitmap_detector_post_call()
    #     self.assertTrue(response.json['message'] == 'non csv file uploaded')
    #     self.assertEqual(response.json['status_code'], 201)

    # # testing output csv file=
    # def test_load_vaidating_output_csv_file(self):
    #     response =  bitmap_detector_post_call()
    #     self.assertTrue(response.json['message'] == 'Bit Map executed Successfully')
    #     self.assertEqual(response.json['status_code'], 201)

    #     # with open(csv_file_path_output):
