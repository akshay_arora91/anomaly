import io
import json
import os
import unittest
import urllib

from werkzeug.datastructures import FileStorage

from app.main import db
from app.test.base import BaseTestCase
from manage import app

from app.main.util.messages import (MOVING_AVG_SUCCESS_MESSAGE,
                                    MANDANTORY_FIELD_REQUIRED_ERROR_MESSAGE,
                                    IP_PATH_OR_FORM_DATA_MANDANTORY_ERROR_MESSAGE,
                                    CSV_IP_PATH_ERROR_MESSAGE,
                                    CSV_OP_PATH_ERROR_MESSAGE)

moving_avg_url = "/api/point_anomaly/moving_avg"
test_client = app.test_client()

current_dir = os.path.dirname(os.path.abspath(__file__))
csv_file_path = os.path.join(current_dir, 'csvfiles/anomaly_input.csv')
csv_file_path_output = os.path.join(
    current_dir, 'csvfiles/moving_avg_output.csv')

# function to urlencode a query string


def get_url_encode(data):
    urlencoded_data = urllib.parse.urlencode(data)
    return urlencoded_data

# function to make post call with parameters


def moving_avg_post_call(ip_path=None, op_path=None, anom_window_size=None, stdn=None, perc=None, urlencode=None, url=None):
    base_payload = dict(
        ip_path=ip_path,
        op_path=op_path
    )
    data = dict(
        anom_window_size=anom_window_size,
        stdn=stdn,
        perc=perc
    )
    response = test_client.post(
        url,
        data=base_payload,
        content_type='multipart/form-data',
        follow_redirects=True,
    )
    print("RESPONSE", response)
    return response


class TestMovingAvg(BaseTestCase):
    # testing status code for a successful post call
    def test_moving_avg_successpost(self):
        anom_window_size = 13
        stdn = 2
        perc = 0.3
        ip_path = csv_file_path
        op_path = csv_file_path_output
        response = moving_avg_post_call(
            ip_path=ip_path,
            op_path=op_path,
            urlencode=False,
            url=f"/api/point_anomaly/moving_avg?deseasonalize=false&data_frequency=weeks&anom_window_size={anom_window_size}&stdn={stdn}&perc={perc}"
        )
        print(response)
        self.assertTrue(response.json['message'] == MOVING_AVG_SUCCESS_MESSAGE)
        self.assertEqual(response.json['status_code'], 201)

    # # test case to check mandatory field
    # def test_moving_avg_mandantory_field(self):
    #     response = moving_avg_post_call(urlencode=True)
    #     self.assertTrue(response.json['message'] ==
    #                     MANDANTORY_FIELD_REQUIRED_ERROR_MESSAGE)
    #     self.assertEqual(response.json['status_code'], 400)

    # # # test case to check whether ip_path or form data upload having none value raise exception
    # def test_moving_avg_ip_path_or_form_data_mandantory(self):
    #     anom_window_size = 13
    #     mov_stdn = 2
    #     perc = 0.3

    #     response = moving_avg_post_call(
    #         urlencode=True,
    #         url=f"/api/point_anomaly/moving_avg?&anom_window_size={anom_window_size}&mov_stdn={mov_stdn}&perc={perc}"
    #     )
    #     self.assertTrue(response.json['message'] ==
    #                     IP_PATH_OR_FORM_DATA_MANDANTORY_ERROR_MESSAGE)
    #     self.assertEqual(response.json['status_code'], 400)

    # # invalid data type for ip_path and op_path
    # def test_moving_avg_invalid_extension_in_path(self):
    #     anom_window_size = 13
    #     ead_stdn = 1
    #     smoothing_factor = 0.5
    #     perc = 0.3
    #     # ip_path csv file validation
    #     response = moving_avg_post_call(
    #         urlencode=True,
    #         url=f"/api/point_anomaly/bit_map?anom_window_size={anom_window_size}\
    #                                 &ead_stdn={ead_stdn}&smoothing_factor={smoothing_factor}\
    #                                 &perc={perc}&ip_path=/home/something"
    #     )
    #     self.assertTrue(response.json['message'] == CSV_IP_PATH_ERROR_MESSAGE)
    #     self.assertEqual(response.json['status_code'], 400)

    #     # op_path csv file validation
    #     response = moving_avg_post_call(
    #         urlencode=True,
    #         url=f"/api/point_anomaly/moving_avg?anom_window_size={anom_window_size}\
    #                                 &ead_stdn={ead_stdn}&smoothing_factor={smoothing_factor}\
    #                                 &perc={perc}"
    #     )
    #     self.assertTrue(response.json['message'] == CSV_OP_PATH_ERROR_MESSAGE)
    #     self.assertEqual(response.json['status_code'], 400)

    # # test case to validate data types in fields
    # def test_moving_avg_uploading_non_csv(self):
    #     response = bitmap_detector_post_call()
    #     self.assertTrue(response.json['message'] == 'non csv file uploaded')
    #     self.assertEqual(response.json['status_code'], 201)

    # # invalid data type for ip_path
    # def test_moving_avg_invalid_extension_in_ip_path(self):
    #     response = bitmap_detector_post_call()
    #     self.assertTrue(response.json['message'] == 'non csv file uploaded')
    #     self.assertEqual(response.json['status_code'], 201)

    # # invalid data type for op_path
    # def test_moving_avg_invalid_extension_in_op_path(self):
    #     response = bitmap_detector_post_call()
    #     self.assertTrue(response.json['message'] == 'non csv file specified')
    #     self.assertEqual(response.json['status_code'], 201)

    # # invalid data type for data
    # def test_moving_avg_uploading_non_csv_file(self):
    #     response = bitmap_detector_post_call()
    #     self.assertTrue(response.json['message'] == 'non csv file uploaded')
    #     self.assertEqual(response.json['status_code'], 201)

    # # uploading csv file with invalid columns
    # def test_moving_avg_uploading_csv_with_invalid_columns(self):
    #     response = bitmap_detector_post_call()
    #     self.assertTrue(response.json['message'] == 'non csv file uploaded')
    #     self.assertEqual(response.json['status_code'], 201)

    # # testing output csv file=
    # def test_moving_avg_vaidating_output_csv_file(self):
    #     response =  bitmap_detector_post_call()
    #     self.assertTrue(response.json['message'] == 'Bit Map executed Successfully')
    #     self.assertEqual(response.json['status_code'], 201)

    #     # with open(csv_file_path_output):
