#Modification done on: 27-Aug-2019 00:35 by Krishna

from abc import ABC, abstractmethod
import types

class PointAnomalyStrategy(ABC):
    """
    Base Class for Point Anomaly algorithms.
    """
    def __init__(self, func=None):
        if func is not None:
            self.execute = types.MethodType(func, self)
            self.fit = types.MethodType(func, self)
            self.predict = types.MethodType(func, self)
            self.name = '{}_{}'.format(self.__class__.__name__, func.__name__)
        else:
            self.name = '{}_default'.format(self.__class__.__name__)

    # Need to be extended.
    @abstractmethod
    def fit(self):
        pass

    # Need to be extended.
    @abstractmethod
    def predict(self):
        pass
