# Modification done on: 13-Feb-2020 12:37 by Amit

import logging
import warnings
from numbers import Integral, Real

import numpy as np
import pandas as pd
from scipy import stats as scs
from werkzeug.exceptions import BadRequest

import AnomalyErrorConstants as er
from PointAnomalyStrategy import PointAnomalyStrategy

logger = logging.getLogger(__name__)


class MovingAverage(PointAnomalyStrategy):
    """This function returns the lower and upper bound calculated
    by performing moving_average and moving standard deviation and
    if the values is below or above these bands respectively, it
    is marked as an anomaly.

    We also cap the values if they are anomalous, so that our bandsśś
    aren't too reactive.

    Attributes
    ----------
    anom_window_size : int, optional(default = 13)
        The window to be considered for determining the next
        point as anomaly.

    stdn : float, optional(default = 2)
        Number of standard deviations, used for moving average.

    perc: float(default=0.30)
        percentage of difference in band and actual value at which capping needs to be done.
        if (perc=1):no capping is done
        if (perc=0):capping is done at the bands
    """

    def __init__(self, anom_window_size=13, stdn=2., perc=0.30):
        self.perc = perc
        self.anom_window_size = anom_window_size
        self.stdn = stdn

        self.__validation_init()

    def raise_bad_request(self, message):
        e = BadRequest(message)
        e.data = {"message": message, "status_code": 400}
        logger.error(message)
        raise e

    def __validation_init(self):
        if not(isinstance(self.stdn, Real)):
            self.raise_bad_request(er.STDN_MESSAGE)

        if not(isinstance(self.perc, Real)):
            self.raise_bad_request(er.PERC_MESSAGE)

        if self.stdn <= 0:
            self.raise_bad_request(er.STDN_ZERO_MESSAGE)

        if ((self.perc < 0) | (self.perc > 1)):
            self.raise_bad_request(er.PERC_ZERO_MESSAGE)

        if not (isinstance(self.anom_window_size, Integral)):
            self.raise_bad_request(er.ANOM_WINDOW_SIZE_INT)

        if self.anom_window_size <= 0:
            self.raise_bad_request(er.ANOM_WINDOW_SIZE_ZERO)

    def __validation_fit(self, data):
        """Validating data frame, data type and number of columns.
        """
        if data.shape[1] < 2:
            self.raise_bad_request(er.DATA_SHAPE_MESSAGE)

        if not(np.issubdtype(data.iloc[:, 1], np.number)):
            self.raise_bad_request(er.DATA_ISSUBDTYPE)

    def __validation_predict(self, data, newpoint):
        """Validating data frame, data type and number of columns.
        """
        kpi = data.columns[1]

        if data.shape[1] < 8:
            self.raise_bad_request(er.DATA_SHAPE_COLUMNS)

        if not(np.issubdtype(data.loc[:, kpi], np.number) and np.issubdtype(data.loc[:, kpi+'_lower'], np.number) and
               np.issubdtype(data.loc[:, kpi+'_upper'], np.number) and np.issubdtype(data.loc[:, kpi+'_outlier'], np.number) and
               np.issubdtype(data.loc[:, kpi+'_p_val'], np.number) and ((data.loc[:, kpi+'_alert_date'].dtype == float) or (
                   data.loc[:, kpi+'_alert_date'].dtype == 'datetime64[ns]') or (data.loc[:, kpi+'_alert_date'].dtype == 'O'))
               and data.loc[:, kpi+'_addnl_info'].dtype == 'O'):
            self.raise_bad_request(er.DATA_SHAPE_COLUMNS_ISSUBDTYPE)

        if newpoint.shape[0] != 1:
            self.raise_bad_request(er.NEWPOINT_ROW_SHAPE)

        if newpoint.shape[1] < 2:
            self.raise_bad_request(er.NEWPOINT_COL_SHAPE)

        if not(np.issubdtype(newpoint.iloc[:, 1], np.number)):
            self.raise_bad_request(er.NEWPOINT_ISSUBDTYPE)

        if ((newpoint.columns[0] not in data.columns) or (newpoint.columns[1] not in data.columns)):
            self.raise_bad_request(er.NEWPOINT_COL_CHECK)

    def __date_format(self, d):
        x = d.copy(deep=True)
        if not isinstance(x, pd.DataFrame):
            self.raise_bad_request(er.DATA_CHECK)

        tim = x.columns[0]
        val_0 = x[tim].values[0]
        try:
            if isinstance(val_0, str):
                if len(val_0) == 10:
                    x[tim] = pd.to_datetime(x[tim], format='%Y-%m-%d')

                elif len(val_0) == 19:
                    x[tim] = pd.to_datetime(x[tim], format='%Y-%m-%d %H:%M:%S')

                else:
                    self.raise_bad_request(er.DATE_FORMAT_CHECK)

            else:
                x[tim] = pd.to_datetime(x[tim], format='%Y-%m-%d %H:%M:%S')
        except Exception as e:
            error_message = " First column should contain date values in yyyy-mm-dd or yyyy-mm-dd HH:MM:SS format. "
            logger.error(error_message)
            self.raise_bad_request(error_message)
        return(x)

    def fit(self, data):
        """This function finds anomaly and returns back a dataframe with
        upper and lower bound along with the outlier(anomaly) flag.

        Parameters
        ----------
        data : pandas.DataFrame
            The dataframe which contains only one column with column
            name as KPI's name and it's values.

        Returns
        -------
        dtr : pandas.DataFrame
            DataFrame with Time,KPI(value of the KPI),KPI_lower(lower band value
            of the KPI),KPI_upper(upper band value of the KPI),KPI_outlier(anomaly
            flag),KPI_p_val(level of significance of the anomaly),KPI_alert_date
            (Date when the anomaly happened),KPI_addnl_info columns(Any additional
            information about the anomaly).
        """

        data = self.__date_format(data)
        self.__validation_fit(data)

        kpi = data.columns[1]
        data1 = data.copy(deep=True)
        data1.sort_values(by=data.columns[0], inplace=True, ascending=True)
        data1.reset_index(drop=True, inplace=True)
        cols = list(data.columns)+[kpi+'_lower', kpi+'_upper', kpi +
                                   '_outlier', kpi+'_p_val', kpi+'_alert_date', kpi+'_addnl_info']

        try:
            if self.anom_window_size >= data1.shape[0]:
                data1[kpi+'_lower'] = np.nan
                data1[kpi+'_upper'] = np.nan
                data1[kpi+'_outlier'] = 0
                data1[kpi+'_p_val'] = 1.0
                data1[kpi+'_alert_date'] = np.nan
                data1[kpi+'_addnl_info'] = [{} for i in range(len(data1))]

                warnings.warn(
                    'Number of rows less than anom_window_size. Setting outlier flag to 0.')
                return (data1[cols])

            x = data1[kpi].values.astype(np.float)
            x_orig = data1[kpi].values.astype(np.float)

            for a in range(len(data1)-self.anom_window_size):
                std = np.std(x[a:a+self.anom_window_size])
                mean = np.mean(x[a:a+self.anom_window_size])
                upper = (mean + self.stdn*std)
                lower = (mean - self.stdn*std)
                lst = x[a + self.anom_window_size]

                if (lst > upper):
                    x[a+self.anom_window_size] = upper + self.perc*(lst-upper)
                elif (lst < lower):
                    x[a+self.anom_window_size] = lower - self.perc*(lower-lst)

            data1[kpi+'_temp'] = x
            mean_ = data1[kpi +
                          '_temp'].rolling(self.anom_window_size).mean().shift(1)
            std_ = data1[kpi+'_temp'].rolling(
                self.anom_window_size).apply(np.std, raw=True).shift(1)

            lower_limit = mean_ - (self.stdn*std_)
            upper_limit = mean_ + (self.stdn*std_)

            outlier = np.where((x_orig > upper_limit) |
                               (x_orig < lower_limit), 1, 0)
            p_val = np.where((np.isnan(mean_) | (std_ == 0)), 1, np.where(x_orig > mean_, 2*(1-scs.norm(loc=mean_, scale=std_).cdf(x=x_orig)),
                                                                          2*scs.norm(loc=mean_, scale=std_).cdf(x=x_orig)))

            data1[kpi+'_lower'] = lower_limit
            data1[kpi+'_upper'] = upper_limit
            data1[kpi+'_outlier'] = outlier
            data1[kpi+'_p_val'] = p_val
            data1[kpi+'_alert_date'] = data1.apply(lambda x: x[data1.columns[0]] if (
                x[kpi+'_outlier'] != 0) else np.nan, axis=1).values
            data1[kpi+'_addnl_info'] = [{} for i in range(len(data1))]

            return (data1[cols])
        except Exception as e:
            logger.error(str(e))

    def predict(self, test, newpoint):
        """This is a scoring function for anomaly detection.

        Parameters
        ----------
        test : pandas.DataFrame
            DataFrame with Time,KPI(value of the KPI),KPI_lower(lower band value
            of the KPI),KPI_upper(upper band value of the KPI),KPI_outlier(anomaly
            flag),KPI_p_val(level of significance of the anomaly),KPI_alert_date
            (Date when the anomaly happened),KPI_addnl_info columns(Any additional
            information about the anomaly). This is typically obtained from fit.
            It is recommended to send same number of data points as the anomaly
            window size.

        newpoint : pandas.DataFrame
            DataFrame which contains two columns with first column as date and
            second with column name as KPI's name and it's values.

        Returns
        -------
        data : pandas.DataFrame
            DataFrame having single row with Time,KPI(value of the KPI),KPI_lower
            (lower band value of the KPI),KPI_upper(upper band value of the KPI),
            KPI_outlier(anomaly flag),KPI_p_val(level of significance of the anomaly)
            ,KPI_alert_date(Date when the anomaly happened),KPI_addnl_info columns
            (Any additional information about the anomaly).
        """

        try:
            test = self.__date_format(test)
            newpoint = self.__date_format(newpoint)
            self.__validation_predict(test, newpoint)

            kpi = newpoint.columns[1]

            test[kpi] = np.where(test[kpi] < test[kpi+'_lower'], test[kpi+'_lower']-self.perc*abs(test[kpi+'_lower']-test[kpi]),
                                 np.where(test[kpi] > test[kpi+'_upper'], test[kpi+'_upper'] + self.perc*abs(test[kpi+'_upper']-test[kpi]),
                                          test[kpi]))

            outdatanp = test.copy(deep=True)

            outdatanp.sort_values(
                by=outdatanp.columns[0], inplace=True, ascending=True)
            outdatanp = outdatanp.append(
                newpoint, ignore_index=True, sort=False)

            outdatanp = outdatanp[list(newpoint.columns)].tail(
                self.anom_window_size+1)

            data = self.fit(outdatanp)

            return (data.loc[data.index[-1]:, :].reset_index(drop=True))
        except Exception as e:
            import os
            import sys
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            error_message = "{}, {}, {}".format(
                exc_type, fname, exc_tb.tb_lineno)
            logger.error(error_message)
            print(error_message)
