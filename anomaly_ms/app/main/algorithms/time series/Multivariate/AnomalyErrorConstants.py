import numpy as np 
import pandas as pd 

############
# Validation init function 

STDN_MESSAGE='stdn should be in numeric(float/int). '
PERC_MESSAGE='perc should be in numeric(float/int). '

# stdn & perc < 0 
STDN_ZERO_MESSAGE ="Invalid value encountered for 'stdn'. Has to be greater than 0."
PERC_ZERO_MESSAGE="Invalid value encountered for 'perc'. Has to be greater than or equal to 0 and less than or equal to 1."

# Smoothing Factor messgaes 
SMOOTHING_FACTOR_REAL='smoothing_factor should be float value between 0 and 1.'
INVALID_SMOOTHING_FACTOR="Invalid value encountered for 'smoothing_factor'. Has to lie between 0 and 1."


# Validation for anomaly window size 
ANOM_WINDOW_SIZE_INT ='anom_window_size should be an integer.'

#anom_window_size <= 0
ANOM_WINDOW_SIZE_ZERO ="Invalid value encountered for 'anom_window_size'. Has to be greater than 0."

############
# Validation Fit function
# data frame shape 
DATA_SHAPE_MESSAGE ='Data should contain two columns, first column should be date and second to be KPI.'

DATA_ISSUBDTYPE='Data should have KPI values in numeric(float/int).'

DATA_SHAPE_ROWS='If expected is None then number of rows should be more than 2.'

############
# Validation predict function
# data frame shape colums should be less than 8 
DATA_SHAPE_COLUMNS='Eight columns Time, kpi, kpi_lower, kpi_upper, kpi_outlier, kpi_p_val, kpi_alert_date, kpi_addnl_info are mandatory .'
DATA_SHAPE_COLUMNS_ISSUBDTYPE="Data should contain the first column as date(Time), followed by five float columns(KPI, KPI_lower, KPI_upper, KPI_outlier, KPI_p_val) with KPI_alert_date being the seventh column(combination of NaNs,NaTs or/and date) and KPI_addnl_info being the last column(of type dictionary)"


# Period Validation 
PERIOD_INT='Invalid variable type encountered, period should be positive integer.'
INVALID_PERIOD="The period length provided is invalid."

# Value Validation
INVALID_VALUE='Invalid value encountered, expected is None or float value.'

# Alpha Value 
ALPHA_VALUE="Invalid value encountered for 'alpha'. Has to be float between 0 and 1."

# MinGap Value
MIN_GAP_VALIDATION="Invalid value encountered for 'min_gap'. Has to be an integer greater than 0."



# newpoint shape 
# newpoint.shape[0] != 1 
NEWPOINT_ROW_SHAPE='The newpoint dataframe should have atleast and atmost one row.'
NEWPOINT_COL_SHAPE='Newpoint should contain two columns, first column should be date and second to be KPI value.'
NEWPOINT_ISSUBDTYPE='The values in second column should be in numeric(float/int).'
NEWPOINT_COL_CHECK='Columns in newpoint not present in test data.'


############
# DateFormat
DATA_CHECK='data should be pandas.Dataframe.'
DATE_FORMAT_CHECK='first column should contain date values in yyyy-mm-dd or yyyy-mm-dd HH:MM:SS format.'
