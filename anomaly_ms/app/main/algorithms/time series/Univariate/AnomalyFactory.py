
class AnomalyFactory(object):
    """
    Base Class for Anomaly algorithm.
    """
    def __init__(self, algo_factory=None):
        self.anomaly_factory = algo_factory
        
    def fit(self):
        algo = self.anomaly_factory()
        algo.fit()
        
    def predict(self):
        algo = self.anomaly_factory()
        algo.predict()