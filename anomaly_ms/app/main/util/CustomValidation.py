from .CustomException import InvalidCSVFile 
import csv

# Validating csv file for anomaly algorithms
def validate_anomaly_input_csv_file(csvfilepath):
    csv_column_list = ["time", "markdown_dollars", "category", "segment", "product"]
    with open(csvfilepath) as fopen:
        csv_reader = csv.reader(fopen)
        # reading headers
        csv_headers = next(csv_reader)
        # checking for number of columns
        if len(csv_headers) < 5:
            raise InvalidCSVFile('csv file uploaded with improper columns')
        # comparing the output csv columns with expected csv columns
        if csv_headers != csv_column_list:
            raise InvalidCSVFile('csv file uploaded with improper columns')

'''
Function to validate anomaly output csv file
Probably would use this function on unit test case validations
'''
# commenting this for now since the output csv might vary
#def validate_anomaly_output_csv_file(csvfilepath):
#    csv_columns_list = ["time", "markdown_dollars", "category", "segment", "product", "markdown_dollars_lower", "markdown_dollars_upper", "markdown_dollars_outlier", "markdown_dollars_p_val", "markdown_dollars_alert_date", "markdown_dollars_addnl_info"]
#    with open(csvfilepath) as fopen:
#        csv_reader = csv.reader(fopen)
#        # getting csv headers
#        csv_headers = next(csv_reader)
#        # checking for number of columns 
#        if len(csv_headers) < 11:
#            raise InvalidCSVFile('output csv file is having invalid columns')
#        if csv_headers != csv_column_list:
#            raise InvalidCSVFile('output csv file is having invalid columns')
#

