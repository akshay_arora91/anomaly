# scoring anomaly algorithms' events
SCORING_ANOMALY_ALGORITHM_EVENT_TYPE = "Scoring Anomaly algorithms"
SCORING_ANOMALY_ALGORITHM_EVENT = "Score Api"

# poing anomaly algorithms' events
POINT_ANOMALY_EVENT_TYPE = "Point Anomaly algorithms"
BITMAP_DETECTOR_EVENT = "Point Anomaly Bitmap Detector Api"
EXP_AVG_EVENT = "Point Anomaly Exponential Average Api"
MOVING_AVG_EVENT = "Point Anomaly Moving Average Api"
RULE_BASED_ANOMALY_EVENT = "Point Anomaly Rule Based Anomaly Api"
RULE_BASED_THREE_BAND_EVENT = "Point Anomaly Rule Based Three Band Api"
SDE_EVENT = "Point Anomaly Sde Api"

# Change anomaly algorithms' events
CHANGE_ANOMALY_EVENT_TYPE = "Change Anomaly algorithms"
CHANGE_POINT_BINSEG_EVENT = "Change Point Binary Segmentation Api"
CHANGE_POINT_BOTTOMUP_EVENT = "Change Point Bottomup Api"
CHANGE_POINT_PELT_EVENT = "Change Point Pelt Api"
CHANGE_POINT_WINDOWS_EVENT = "Change Point Windows Api"
CHANGE_POINT_DYNP_EVENT = "Change Point Dynp Api"

# Run anomaly algorithms' events
RUN_ANOMALY_EVENT_TYPE = "Run Anomaly algorithms"
MONOTONIC_LEN_EVENT = "Run Monotonic Len Api"

# Trend anomaly algorithms' events
TREND_ANOMALY_EVENT_TYPE = "Trend Anomaly algorithms"
TREND_EVENT = "Trend Api"

# No anomaly algorithms' events
NO_ANOMALY_EVENT_TYPE = "No Anomaly algorithms"
NO_ANOMALY_EVENT = "No Anomaly Api"

# Change Trend algorithms' events
CHANGE_TREND_EVENT_TYPE = "Change Trend algorithms"
CHANGE_TREND_EVENT = "Change Trend Api"

# Change distribution algorithms' events
CHANGE_DISTRIBUTION_EVENT_TYPE = "Change Distribution algorithms"
CHANGE_DISTRIBUTION_EVENT = "Change Distribution Api"

# peer to peer anomaly algorithms' events
PEER_TO_PEER_EVENT_TYPE = "Peer to Peer algorithms"
PEER_TO_PEER_EVENT = "Peer To Peer Macro Api"

PREDICTION_PROPERTY_NAME = "Prediction"

SCORING_PREDICT_PROPERTY_VALUE = "Scoring Anomaly predict"
POINT_PREDICT_PROPERTY_VALUE = "Point Anomaly predict"
CHANGE_PREDICT_PROPERTY_VALUE = "Change Anomaly predict"
RUN_PREDICT_PROPERTY_VALUE = "Run Anomaly predict"
TREND_PREDICT_PROPERTY_VALUE = "Trend Anomaly predict"
CHANGE_DISTRIBUTION_PROPERTY_VALUE = "Change Distribution Anomaly predict"
PEER_PROPERTY_VALUE = "Peer to Peer Anomaly predict"
NO_ANOMALY_PROPERTY_VALUE = "No Anomaly predict"

TRAINING_PROPERTY_NAME = "Training"
SCORING_TRAIN_PROPERTY_VALUE = "Scoring Anomaly train"
POINT_TRAIN_PROPERTY_VALUE = "Point Anomaly train"
CHANGE_TRAIN_PROPERTY_VALUE = "Change Anomaly train"
RUN_TRAIN_PROPERTY_VALUE = "Run Anomaly train"
TREND_TRAIN_PROPERTY_VALUE = "Trend Anomaly train"
CHANGE_DISTRIBUTION_TRAIN_PROPERTY_VALUE = "Change Distribution Anomaly train"
PEER_PROPERTY_TRAIN_VALUE = "Peer to Peer Anomaly train"
NO_ANOMALY_PROPERTY_TRAIN_VALUE = "No Anomaly train"