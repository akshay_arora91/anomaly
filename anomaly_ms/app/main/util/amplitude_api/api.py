import requests

class Amplitude:
	# def __init__(self, base_url=None, api_key=None, event_type=None, user_id=None):
	# 	if base_url:
	# 		self.base_url = base_url
	# 	if api_key:
	# 		self.api_key = api_key
	# 	if event_type:	
	# 		self.event_type = event_type
	# 	if user_id:	
	# 		self.user_id = user_id

	def post_events(self, base_url=None, api_key=None, event_type=None, user_id=None):
		self.base_url = base_url
		self.api_key = api_key
		self.event_type = event_type
		self.user_id = user_id

		headers = dict()
		headers["Content-Type"] = "application/json"
		data = dict()
		data["api_key"] = self.api_key
		data["events"] = []
		data["events"].append({"user_id":self.user_id, "event_type":self.event_type})
		response = requests.post(self.base_url, headers=headers, data=str(data))	
		return response.text

