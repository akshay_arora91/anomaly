from flask_restplus import Namespace, fields, reqparse, inputs
import werkzeug

'''
Anomaly-
    -Moving Average
    -Exponential Average Detector
    -SDE
    -Bitmap Detector
    -Rule based Anomaly
          - Change from prev value
          - change from mean
          - change from median
          - Threshold value
          - Threshhold percentage
          - Contextual Anomaly
'''


anomaly_algorithm = reqparse.RequestParser()
anomaly_algorithm.add_argument('data',
                               type=werkzeug.datastructures.FileStorage,
                               location='files',
                               help='CSV file')
anomaly_algorithm.add_argument(
    'deseasonalize', type=inputs.boolean, help='Deseasonalize the data', default=False)
anomaly_algorithm.add_argument(
    'ip_path', type=str, help='Optional Input file', location='form')
anomaly_algorithm.add_argument(
    'op_path', type=str, help='Optional Output file', location='form')
anomaly_algorithm.add_argument(
    'data_source_id', type=str, help='Data Source ID', location='form')
anomaly_algorithm.add_argument(
    'project_id', type=str, help='Project ID', location='form')
anomaly_algorithm.add_argument(
    'experiment_id', type=str, help='Experiment ID', location='form')
anomaly_algorithm.add_argument(
    'request_id', type=str, help='Request ID', location='form')
anomaly_algorithm.add_argument(
    'user_id', type=str, help='User ID', location='form')
anomaly_algorithm.add_argument('data_frequency', type=str,  choices=('days', 'weeks', 'months', 'years'), default='weeks',
                               help='Frequency of Data')


class ChangeAlgorithmDto:
    change_anomaly_api = Namespace(
        'Change Anomaly', description='Change Anomaly Algorithms')
    changepoint_dynp = anomaly_algorithm.copy()
    changepoint_dynp.add_argument(
        'window_size', type=int, help='window_size', default=60)
    changepoint_dynp.add_argument(
        'min_gap', type=int, help='min_gap', default=5)
    changepoint_dynp.add_argument(
        'model', type=str, help='model', default="rbf")
    changepoint_dynp.add_argument('lbw', type=int, help='lbw', default=5)
    changepoint_dynp.add_argument('bkpts', type=int, help='bkpts', default=3)

    change_point_pelt = anomaly_algorithm.copy()
    change_point_pelt.add_argument(
        'window_size', type=int, help='window_size', default=30)
    change_point_pelt.add_argument(
        'min_gap', type=int, help='min_gap', default=5)
    change_point_pelt.add_argument(
        'model', type=str, help='model', default="rbf")
    change_point_pelt.add_argument('lbw', type=int, help='lbw', default=5)
    change_point_pelt.add_argument(
        'penalty', type=float, help='penalty', default=2.5)

    change_point_binseg = anomaly_algorithm.copy()

    change_point_binseg.add_argument(
        'window_size', type=int, help='window_size', default=30)
    change_point_binseg.add_argument(
        'min_gap', type=int, help='min_gap', default=5)
    change_point_binseg.add_argument(
        'model', type=str, help='model', default="rbf")
    change_point_binseg.add_argument('lbw', type=int, help='lbw', default=5)
    change_point_binseg.add_argument(
        'bkpts', type=int, help='bkpts', default=3)
    change_point_binseg.add_argument(
        'penalty', type=float, help='penalty', default=2.5)
    change_point_binseg.add_argument('eps', type=float, help='eps')

    change_point_windows = anomaly_algorithm.copy()

    change_point_windows.add_argument(
        'window_size', type=int, help='window_size', default=60)
    change_point_windows.add_argument(
        'min_gap', type=int, help='min_gap', default=5)
    change_point_windows.add_argument(
        'width', type=int, help='model', default=20)
    change_point_windows.add_argument(
        'model', type=str, help='model', default="rbf")
    change_point_windows.add_argument('lbw', type=int, help='lbw', default=5)
    change_point_windows.add_argument(
        'bkpts', type=int, help='bkpts', default=3)
    change_point_windows.add_argument(
        'penalty', type=float, help='penalty', default=2.5)
    change_point_windows.add_argument('eps', type=float, help='eps')

    change_point_bottomup = anomaly_algorithm.copy()

    change_point_bottomup.add_argument(
        'window_size', type=int, help='anom_window_size', default=30)
    change_point_bottomup.add_argument(
        'min_gap', type=int, help='min_gap', default=5)
    change_point_bottomup.add_argument(
        'model', type=str, help='model', default='rbf')
    change_point_bottomup.add_argument('lbw', type=int, help='lbw', default=5)
    change_point_bottomup.add_argument(
        'bkpts', type=int, help='bkpts', default=3)
    change_point_bottomup.add_argument(
        'penalty', type=float, help='penalty', default=2.5)
    change_point_bottomup.add_argument(
        'eps', type=float, help='eps')


class ScoringAnomalyAlgorithmDto:
    api = Namespace('Scoring Anomaly Algorithms',
                    description='Scoring Anomaly Algorithms')
    pickle_load = reqparse.RequestParser()
    pickle_load.add_argument('historical',
                             type=werkzeug.datastructures.FileStorage,
                             location='files',
                             help='Historical Data')
    pickle_load.add_argument('newpoint',
                             type=werkzeug.datastructures.FileStorage,
                             location='files',
                             help='New Point. Only One row')
    pickle_load.add_argument(
        'pickle', type=str, help='Pickle Byte String Returned from any of the algorithm API')


class PointAnomalyAlgorithmDto:
    api = Namespace('Point Anomaly',
                    description='Point Anomaly Algorithms')
    moving_average = anomaly_algorithm.copy()
    moving_average.add_argument(
        'anom_window_size', type=int, help='Window Size', default=13)
    moving_average.add_argument(
        'stdn', type=float, help='Moving Standard Deviation', default=2)
    moving_average.add_argument(
        'perc', type=float, help='Percentage of difference in band and actual value at which capping needs to be done.', default=0.3)

    bitmap_detector = anomaly_algorithm.copy()
    bitmap_detector.add_argument(
        'stdn', type=float, help='Bit Standard Deviation', default=2)

    exp_avg = anomaly_algorithm.copy()

    exp_avg.add_argument('anom_window_size', type=int,
                         help='Anomaly Window Size', default=13)
    exp_avg.add_argument('stdn', type=float, help='EAD Stdn', default=2.5)
    exp_avg.add_argument('smoothing_factor', type=float,
                         help='Smoothing Factor', default=0.3)
    exp_avg.add_argument(
        'perc', type=float, help='Percentage of difference in band and actual value at which capping needs to be done.', default=0.3)

    sde = anomaly_algorithm.copy()

    sde.add_argument('anom_window_size', type=int,
                     help='Anomaly Window Size', default=13)
    sde.add_argument('alpha', type=float, help='Alpha', default=0.05)
    sde.add_argument('iterations', type=int, help='Iterations', default=10000)
    sde.add_argument('error', type=str, help='Error', default="RMSE")
    sde.add_argument('method', type=str, help='Method')
    sde.add_argument('perc', type=float,
                     help='Percentage of difference in band and actual value at which capping needs to be done.', default=0.3)

    rule_based_anomaly = anomaly_algorithm.copy()

    rule_based_anomaly.add_argument(
        'anom_window_size', type=int, help='Anomaly Window Size', default=13)
    rule_based_anomaly.add_argument(
        'process', type=str, help='process', default="mean")
    rule_based_anomaly.add_argument(
        'perc_change_val', type=float, help='perc_change_val', default=10)
    rule_based_anomaly.add_argument(
        'band_len_upp', type=float, help='band_len_upp')
    rule_based_anomaly.add_argument(
        'band_len_low', type=float, help='band_len_low')
    rule_based_anomaly.add_argument(
        'alpha', type=float, help='alpha', default=0.05)

    rule_based_three_band_anomaly = anomaly_algorithm.copy()

    rule_based_three_band_anomaly.add_argument(
        'upper', type=inputs.boolean, help='desired side of KPI', default=True)
    rule_based_three_band_anomaly.add_argument(
        'greenThreshold', type=float, help='This is the threshold value for Percentage Change for Green Flag', default=0)
    rule_based_three_band_anomaly.add_argument(
        'amberThreshold', type=float, help='This is the threshold value for Percentage Change for Amber Flag', default=5)
    rule_based_three_band_anomaly.add_argument(
        'redThreshold', type=float, help='This is the threshold value for Percentage Change for Red Flag', default=10)
    rule_based_three_band_anomaly.add_argument(
        'alpha', type=float, help='Significance level for P value computation.', default=0.05)


class TrendAlgorithmDto:
    api = Namespace('Trend Anomaly', description='Trend Algorithms')
    trend = anomaly_algorithm.copy()

    trend.add_argument('alpha', type=float, help='alpha', default=0.05)
    trend.add_argument('window_size', type=int, help='window_size')
    trend.add_argument('min_gap', type=int, help='min_gap', default=5)
    trend.add_argument('method', type=str, help='method', default="cox")

    custom_trend = anomaly_algorithm.copy()

    custom_trend.add_argument(
        'percentage_change', type=float, help='percentage_change', default=30.0)
    custom_trend.add_argument('window_size', type=int,
                              help='window_size', default=8)
    custom_trend.add_argument('direction', type=str,
                              help='direction', default="positive")


class RunAlgorithmDto:
    api = Namespace('Run Anomaly',
                    description='Run Anomaly Algorithms')
    monotonic_len = anomaly_algorithm.copy()

    monotonic_len.add_argument('periods', type=int, help='Period', default=10)
    monotonic_len.add_argument('alpha', type=float, help='alpha', default=0.05)
    monotonic_len.add_argument('min_gap', type=int, help='min_gap', default=5)
    monotonic_len.add_argument('expected', type=float, help='expected')


class NoAnomalyAlgorithmDto:
    api = Namespace('No Anomaly',
                    description='No Anomaly Algorithms')
    no_anomaly = anomaly_algorithm.copy()


class ChangeTrendAlgorithmDto:
    api = Namespace('Change Trend',
                    description='Change Trend Algorithms')
    change_trend = anomaly_algorithm.copy()

    change_trend.add_argument(
        'rolling_window', type=int, help='rolling_window', default=30)
    change_trend.add_argument('alert_window', type=int,help='alert_window')
    change_trend.add_argument('stdn', type=int, help='stdn', default=1)
    change_trend.add_argument('smoothening_window',
                              type=int, help='smoothening_window', default=24)


class ChangeDistributionDto:
    api = Namespace('Change Distribution',
                    description='Change Distribution Algorithms')
    change_distribution = anomaly_algorithm.copy()

    change_distribution.add_argument(
        'window', type=int, help='window', default=40)
    change_distribution.add_argument('bins', type=int,
                                     help='bins', default=5)
    change_distribution.add_argument(
        'alpha', type=float, help='alpha', default=0.05)
    change_distribution.add_argument('min_gap',
                                     type=int, help='min_gap', default=10)


class PeertoPeerAlgorithmDto:
    api = Namespace('Peer to Peer',
                    description='Peer to Peer Anomaly Algorithms')
    api_beta = Namespace('Peer to Peer Beta',
                    description='Peer to Peer Beta Anomaly Algorithms')
    
    macro = anomaly_algorithm.copy()

    macro.add_argument(
        'Group_by', action='append', help='Group the data by')
    macro.add_argument(
        'multiplier', type=int, help='multiplier', default=1)
    macro.add_argument('method', type=str,  choices=('med', 'iqr'), default='med',
                       help='Peer to Peer method')

    type_two_macro = anomaly_algorithm.copy()
    type_two_macro.add_argument(
        'hierarchy', type=str, help='hierarchy')

    type_two_macro.add_argument(
        'baseline_hierarchy', type=str, help='baseline_hierarchy')
    type_two_macro.add_argument(
        'baseline_data_path', type=str, help='baseline data Input file', location='form')

    type_two_macro.add_argument(
        'hierarchy_set', type=str, help='hierarchy_set', location='form')

    type_two_macro.add_argument(
        'window', type=int, help='window', default=3)
    type_two_macro.add_argument(
        'radius', type=int, help='radius', default=3)

    type_one_macro = anomaly_algorithm.copy()
    type_one_macro.add_argument(
        'group_by_column', type=str, help='group_by_column')

    type_one_macro.add_argument(
        'hierarchy', type=str, help='hierarchy')
    type_one_macro.add_argument(
        'distance_method', type=str, help='distance_method', choices=('dtw', 'cid', 'euclidean', 'chebyshev'), default='cid')

    type_one_macro.add_argument(
        'rule', type=inputs.boolean, help='rule', default=False)

    type_one_macro.add_argument(
        'window', type=int, help='window', default=3)
    type_one_macro.add_argument(
        'stdn', type=float, help='stdn', default=3)

    type_one_macro.add_argument(
        'clus_window', type=int, help='clus_window', default=3)

    type_one_macro.add_argument(
        'min_gap', type=int, help='min_gap', default=2)

    type_one_macro.add_argument(
        'mov_window', type=int, help='mov_window', default=30)
    type_one_macro.add_argument(
        'anomaly_method', type=str, help='anomaly_method', default='cluster')

    type_one_macro.add_argument(
        'smoothing', type=inputs.boolean, help='smoothing', default=False)

    type_one_macro.add_argument(
        'smoothing_window', type=int, help='smoothing_window', default=4)

    type_one_macro.add_argument(
        'smoothing_weight', type=float, help='smoothing_weight', default=0.8)
    type_one_macro.add_argument(
        'series_count', type=int, help='series_count', default=8)
