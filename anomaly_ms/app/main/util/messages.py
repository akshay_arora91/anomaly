'''
This py file will contains all the success and exception messages which would be used along 
'''

# Success Messages
# Point Anomaly Algorithms' success messages
BITMAP_DETECTOR_SUCCESS_MESSAGE = "Bitmap Executed Successfully"
EXP_AVG_SUCCESS_MESSAGE = "Exponential average Executed Successfully"
LOAD_SUCCESS_MESSAGE = "Exported Successfully"
MOVING_AVG_SUCCESS_MESSAGE = "Moving Average Executed Successfully"
RULE_BASED_ANOMALY_SUCCESS_MESSAGE = "Rule Based Anomaly Executed Successfully"
RULE_BASED_THREE_BAND_SUCCESS_MESSAGE = "Rule Based Three Band Executed Successfully"
SDE_SUCCESS_MESSAGE = "Sde Executed Successfully"

# Change Anomaly Algorithms' success messages
CHANGE_POINT_BINSEG_SUCCESS_MESSAGE = "Changepoint Binseg Executed Successfully"
CHANGE_POINT_BOTTOMUP_SUCCESS_MESSAGE = "Changepoint Bottomup Executed Successfully"
CHANGE_POINT_PELT_SUCCESS_MESSAGE = "Changepoint Pelt Executed Successfully"
CHANGE_POINT_WINDOWS_SUCCESS_MESSAGE = "Changepoint Windows Executed Successfully"
CHANGE_POINT_DYNP_SUCCESS_MESSAGE = "Changepoint Dynp Executed Successfully"
CHANGE_TREND_SUCCESS_MESSAGE = "Change Trend Executed Successfully"
CHANGE_DISTRIBUTION_SUCCESS_MESSAGE = "Change Distribution Executed Successfully"
# Run Anomaly Algorithms' success messages
MONOTONIC_LEN_SUCCESS_MESSAGE = "MonotonicLen Executed Successfully"

# Trend Anomaly Algorithms' success messages
TREND_SUCCESS_MESSAGE = "Trend Executed Successfully"

# No Anomaly Algorithms' success messages
NO_ANOMALY_SUCCESS_MESSAGE = "Trend Executed Successfully"

MACRO_SUCCESS_MESSAGE = "FS Algo Executed Successfully"
TYPE_TWO_MACRO_SUCCESS_MESSAGE = "PeerToPeer TypeTwo Algo Executed Successfully"
TYPE_ONE_MACRO_SUCCESS_MESSAGE = "PeerToPeer TypeOne Algo Executed Successfully"

CUSTOM_TREND_SUCCESS_MESSAGE = "Custom Trend Executed Successfully"
# ====================================================================================================
# Exception Messages
CSV_IMPROPER_COLUMNS_ERROR_MESSAGE = "csv file uploaded with improper columns"
CSV_IP_PATH_ERROR_MESSAGE = "csv file not mentioned in ip_path"
CSV_OP_PATH_ERROR_MESSAGE = "csv file not mentioned in op_path"
IP_PATH_OR_FORM_DATA_MANDANTORY_ERROR_MESSAGE = "Atleast one parameter should be given. Path or FormData file"
MANDANTORY_FIELD_REQUIRED_ERROR_MESSAGE = "Mandantory field required"
RULE_BASED_THREE_BAND_FAILED_MESSAGE = "Rule Based Three Band Execution Failed"
CUSTOM_TREND_FAILED_MESSAGE = "Custom Trend Execution Failed"
