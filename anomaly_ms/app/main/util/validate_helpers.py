import pandas as pd
from werkzeug.exceptions import BadRequest
import numpy as np


def validate_df(data):
    if not(isinstance(data, pd.DataFrame)):
        e = BadRequest('Data should be pandas.DataFrame')
        e.data = {"message": "Data should be pandas.DataFrame",
                  "status_code": 400}
        raise e


def validate_df_shape(data):
    if data.shape[1] < 2:
        e = BadRequest()
        e.data = {
            "message": "Data should contain two columns, first column should be date and second to be KPI", "status_code": 400}
        raise e


def validate_df_val_format(data):
    if not(isinstance(pd.to_datetime(data.iloc[0, 0]), pd.datetime) and (isinstance(data.iloc[0, 1], float) or isinstance(data.iloc[0, 1], np.integer))):
        e = BadRequest('Data should contain date and float columns')
        e.data = {
            "message": "Data should contain date and float columns", "status_code": 400}
        raise e


def validate_no_anomaly(data):
    validate_df(data)
    validate_df_shape(data)


def validate_mvng_avg(data, anom_window_size, stdn, perc):
    validate_df(data)
    validate_df_shape(data)
    if not(isinstance(stdn, float) or isinstance(stdn, int)):
        e = BadRequest('Data should contain date and float columns')
        e.data = {
            "message": "Data should contain date and float columns", "status_code": 400}
        raise e

    if not(isinstance(perc, float) or isinstance(perc, int)):
        e = BadRequest('perc should be float')
        e.data = {"message": "perc should be float", "status_code": 400}
        raise e

    if ((perc < 0) | (perc > 1)):
        e = BadRequest(
            "Invalid value encountered for 'perc'. Has to be greater than 0 and less than1")
        e.data = {
            "message": "Invalid value encountered for 'perc'. Has to be greater than 0 and less than1", "status_code": 400}
        raise e

    if not isinstance(anom_window_size, int):
        e = BadRequest('anom_window_size should be integer')
        e.data = {"message": "anom_window_size should be integer",
                  "status_code": 400}
        raise e
    if anom_window_size <= 0:
        e = BadRequest(
            "Invalid value encountered for 'anom_window_size'. Has to be greater than 0")
        e.data = {
            "message": "Invalid value encountered for 'anom_window_size'. Has to be greater than 0", "status_code": 400}
        raise e
    if stdn <= 0:
        e = BadRequest(
            "Invalid value encountered for 'stdn'. Has to be greater than 0")
        e.data = {
            "message": "Invalid value encountered for 'stdn'. Has to be greater than 0", "status_code": 400}
        raise e

    # validate_df_val_format(data)
    # if data.shape[0]<=anom_window_size:
    #     e = BadRequest('Data should have more than anom_window_size rows')
    #     e.data = {"message":"Data should have more than anom_window_size rows","status_code" : 400}
    #     raise e


def validate_bit_map(data):
    validate_df(data)
    validate_df_shape(data)
    validate_df_val_format(data)


def validate_exp_avg(data, anom_window_size, perc):
    validate_df(data)
    validate_df_shape(data)
    if data.shape[0] <= anom_window_size:
        e = BadRequest('Data should have more than anom_window_size rows')
        e.data = {
            "message": "Data should have more than anom_window_size rows", "status_code": 400}
        raise e
    validate_df_val_format(data)


def validate_sde(data, perc):
    validate_df(data)
    validate_df_shape(data)
    validate_df_val_format(data)


def validate_rule_based(data, anom_window_size, process, perc_change_val, band_len_upp, band_len_low, alpha):
    validate_df(data)
    validate_df_shape(data)
    if data.shape[0] <= anom_window_size:
        e = BadRequest('Data should have more than anom_window_size rows')
        e.data = {
            "message": "Data should have more than anom_window_size rows", "status_code": 400}
        raise e
    validate_df_val_format(data)
    if not(isinstance(alpha, int) or isinstance(alpha, float)):
        raise BadRequest(
            'Invalid value encountered for alpha, expecting float value')
    if (alpha >= 1) or (alpha <= 0):
        raise BadRequest(
            'Invalid value encountered for alpha, expecting float value between (0,1)')
    if (not isinstance(anom_window_size, int)) or (anom_window_size <= 0):
        raise BadRequest(
            'Invalid value encountered for anom_window_size, integer value greater than 0 expected')
    if (not isinstance(process, str)) or (process not in ['mean', 'median', 'prev_val', 'threshold']):
        raise BadRequest(
            'Invalid value encountered for process, string from mean, median, prev_val, threshold expected')
    if not(isinstance(perc_change_val, int) or isinstance(perc_change_val, float)):
        raise BadRequest(
            'Invalid value encountered for perc_change_val, expecting float value')
    if perc_change_val < 0:
        raise BadRequest(
            "Invalid value encountered for 'perc_change_val'. Has to be greater than 0")


def validate_change_point_bin_seg(data, window_size, min_gap, model, lbw, bkpts, penalty, eps):
    validate_df(data)
    validate_df_shape(data)
    validate_df_val_format(data)
    if (not isinstance(window_size, int)) or (window_size <= 0):
        raise BadRequest(
            "Invalid value encountered for 'window_size'. Has to be integer greater than 0")
    if not isinstance(lbw, int):
        raise BadRequest('lbw should be integer')
    if not isinstance(bkpts, int):
        raise BadRequest('bkpts should be integer')
    if not isinstance(min_gap, int):
        raise BadRequest('min_gap should be integer')
    if min_gap <= 0:
        raise BadRequest(
            "Invalid value encountered for 'min_gap'. Has to be greater than 0")
    if (not isinstance(penalty, float)) and (not isinstance(penalty, int)):
        raise BadRequest('penalty should be float value')
    if not isinstance(model, str):
        raise BadRequest('model should be string')
    if model not in ['l1', 'l2', 'rbf']:
        raise BadRequest(
            'Invalid model selected, Should be one of the following :l1 or l2 or rbf')

    if min_gap >= window_size:
        raise BadRequest(
            'Invalid value encuntered, min_gap should be smaller than window_size')


def validate_monotonic(data, period):
    validate_df(data)
    validate_df_shape(data)
    validate_df_val_format(data)
    if not isinstance(period, int):
        raise BadRequest(
            'Invalid variable type encountered, period should be positive integer')
    elif period <= 0:
        raise BadRequest("The period length provided is invalid.")


def validate_cp_def(data, window_size, min_gap, model, lbw, bkpts):
    validate_df(data)
    validate_df_shape(data)
    validate_df_val_format(data)
    if (not isinstance(window_size, int)) or (window_size <= 0):
        raise BadRequest(
            "Invalid value encountered for 'window_size'. Has to be integer greater than 0")
    if not isinstance(min_gap, int):
        raise BadRequest('min_gap should be integer')
    if min_gap < 0:
        raise BadRequest('min_gap should be integer greater than 0')
    if not isinstance(bkpts, int):
        raise BadRequest('bkpts should be integer')
    if not isinstance(model, str):
        raise BadRequest('model should be string')
    if model not in ['l1', 'l2', 'rbf']:
        raise BadRequest(
            'Invalid model selected, Should be one of the following :l1 or l2 or rbf')

    if min_gap >= window_size:
        raise BadRequest(
            'Invalid value encuntered, min_gap should be smaller than window_size')


def validate_changepoint_dynp(data, window_size, min_gap, model, lbw, bkpts):
    validate_df(data)
    validate_df_shape(data)
    validate_df_val_format(data)
    if (not isinstance(window_size, int)) or (window_size <= 0):
        raise BadRequest(
            "Invalid value encountered for 'window_size'. Has to be integer greater than 0")
    if not isinstance(min_gap, int):
        raise BadRequest('min_gap should be integer')
    if min_gap < 0:
        raise BadRequest('min_gap should be integer greater than 0')
    if not isinstance(bkpts, int):
        raise BadRequest('bkpts should be integer')
    if not isinstance(model, str):
        raise BadRequest('model should be string')
    if model not in ['l1', 'l2', 'rbf']:
        raise BadRequest(
            'Invalid model selected, Should be one of the following :l1 or l2 or rbf')

    if min_gap >= window_size:
        raise BadRequest(
            'Invalid value encuntered, min_gap should be smaller than window_size')


def validate_changepoint_pelt(data, window_size, min_gap, model, lbw, penalty):
    validate_df(data)
    validate_df_shape(data)
    validate_df_val_format(data)
    if (not isinstance(window_size, int)) or (window_size <= 0):
        raise BadRequest(
            "Invalid value encountered for 'window_size'. Has to be integer greater than 0")
    if not isinstance(min_gap, int):
        raise BadRequest('min_gap should be integer')
    if min_gap < 0:
        raise BadRequest('min_gap should be integer greater than 0')
    if not isinstance(model, str):
        raise BadRequest('model should be string')
    if model not in ['l1', 'l2', 'rbf']:
        raise BadRequest(
            'Invalid model selected, Should be one of the following :l1 or l2 or rbf')

    if min_gap >= window_size:
        raise BadRequest(
            'Invalid value encuntered, min_gap should be smaller than window_size')


def validate_changepoint_dynp(data, window_size, min_gap, model, lbw, bkpts):
    validate_df(data)
    validate_df_shape(data)
    validate_df_val_format(data)
    if (not isinstance(window_size, int)) or (window_size <= 0):
        raise BadRequest(
            "Invalid value encountered for 'window_size'. Has to be integer greater than 0")
    if not isinstance(min_gap, int):
        raise BadRequest('min_gap should be integer')
    if min_gap < 0:
        raise BadRequest('min_gap should be integer greater than 0')
    if not isinstance(model, str):
        raise BadRequest('model should be string')
    if model not in ['l1', 'l2', 'rbf']:
        raise BadRequest(
            'Invalid model selected, Should be one of the following :l1 or l2 or rbf')

    if min_gap >= window_size:
        raise BadRequest(
            'Invalid value encuntered, min_gap should be smaller than window_size')


def validate_trend(data, alpha, min_gap, method):
    validate_df(data)
    validate_df_shape(data)
    validate_df_val_format(data)

    if (alpha <= 0) or (alpha >= 1):
        raise BadRequest(
            "Invalid value encountered for 'alpha'. Has to be in between 0 and 1")

    if min_gap <= 0:
        raise BadRequest(
            "Invalid value encountered for 'min_gap'. Has to be greater than 0")

    if method not in ['cox', 'binomial']:
        raise BadRequest(
            "Invalid method type. Should be one of the following : cox/binomial")
