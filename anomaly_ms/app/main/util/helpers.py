from werkzeug.exceptions import BadRequest


def raise_bad_request(message):
    e = BadRequest(message)
    e.data = {"data": None, "message": message, "status_code": 400}
    raise e
